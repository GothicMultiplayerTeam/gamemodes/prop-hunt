local function player_ids(players) {
    local ids = array(players.len(), 0)
    foreach (i, player in players) {
        ids[i] = player.id
    }

    return ids
}

class RoundState extends BState {
    _update_timer_ = -1
    _last_time_ms = -1
    _time_left_ms = -1

    _players = null
    _taunts = null
    _map = null
    _area = null
    _stage = null

    constructor(map) {
        this._map = map
        this._area = Area({ points = this._map.area.points, world = this._map.world })
        this._stage = StageType.HIDING

        this._last_time_ms = getTickCount()
        this._time_left_ms = this._map.round.hiding * 1000
    }

    function _assignPlayersClasses(players) {
        local total_players = players.len()
        local hunter_players = max(1, floor(total_players * 0.4))
        local prop_players = total_players - hunter_players

        local elements = clone(players)
        randomShuffle(elements)

        local hunters = []
        for (local i = 0; i < hunter_players; ++i) {
            elements[i].assigned_class = PlayerType.HUNTER
            hunters.push(elements[i])
        }

        local props = []
        for (local i = hunter_players; i < total_players; ++i) {
            elements[i].assigned_class = PlayerType.PROP
            props.push(elements[i])
        }

        return {hunters = hunters, props = props}
    }

    function _teleportPlayers(players, offset) {
        local spawn = this._map.spawn
        foreach (player in players) {
            local offset_x = (rand() % offset) - offset / 2
            local offset_z = (rand() % offset) - offset / 2

            setPlayerPosition(player.id, spawn.x + offset_x, spawn.y, spawn.z + offset_z)
        }
    }

    function _switchStageToHunting() {
        this._stage = StageType.HUNTING
        this._time_left_ms = this._map.round.hunting * 1000

        this._teleportPlayers(this._players.hunters, 500)
        foreach (player in this._players.hunters) {
            spawnPlayer(player.id)
        }
        
        local sync = RoundSynchronization(player_ids(server.players.online))
        sync.updateState({time_left_ms = this._time_left_ms, stage_type = StageType.HUNTING})
    }

    function _validatePlayersArea() {
        foreach (player in server.players.online) {
            local position = getPlayerPosition(player.id)
            local players = []

            if (position.y < this._map.area.min_height || position.y > this._map.area.max_height) {
                players.push(player)
            } else if (!this._area.isIn(position.x, position.y, position.z, getPlayerWorld(player.id))) {
                players.push(player)
            }

            foreach (player in players) {
                if (this._stage == StageType.HUNTING && player.assigned_class == PlayerType.PROP) {
                    local health = getPlayerHealth(player.id) - PROP_EXIT_AREA_DAMAGE
                    setPlayerHealth(player.id, health >= 0 ? health : 0)
                }

                sendMessageToPlayer(player.id, 255, 0, 0, "Don't go outside game area!")
            }

            this._teleportPlayers(players, 500)
        }
    }

    /// STAGE: Hiding
    function _updateStageHiding(delta_ms) {
        if (this._players.props.len() == 0) {
            this.machine.change(SummaryState(PlayerType.HUNTER))
        } else if (this._players.hunters.len() == 0) {
            this.machine.change(SummaryState(PlayerType.PROP))
        }
    }

    /// STAGE: Hunting
    function _updateStageHunting(delta_ms) {
        if (this._players.props.len() == 0) {
            this.machine.change(SummaryState(PlayerType.HUNTER))
        } else if (this._players.hunters.len() == 0) {
            this.machine.change(SummaryState(PlayerType.PROP))
        } else {
            this._taunts.update(delta_ms)
        }
    }

    function onEnter() {
        this._update_timer_ = setTimer(this.onUpdate.bindenv(this), 200, 0)

        // Assign classes
        this._players = this._assignPlayersClasses(server.players.online)
        this._taunts = TauntsManager(this._players, PROP_TAUNT_PERIODICITY_SEC)

        foreach (player in server.players.online) {
            player.vob_index = 0 // Reset vob index
            classes.Setup(player)
        }

        this._teleportPlayers(server.players.online, 500)
        // Spawn only props in hiding stage
        foreach (player in this._players.props) {
            spawnPlayer(player.id)
        }

        local sync = RoundSynchronization(player_ids(server.players.online))
        sync.changeState(this._stage, this._map, this._time_left_ms, this._players.hunters.len(), this._players.props.len())
        sync.propsCreate(this._players.props)
    }

    function onExit() {
        if (this._update_timer_ != -1) {
            killTimer(this._update_timer_)
            this._update_timer_ = -1
        }

        // Clear players
        foreach (player in server.players.online) {
            player.assigned_class = null
        }
    }

    function onUpdate() {
        // Go back to lobby if there are no players
        if (server.players.online.len() == 0) {
            this.machine.change(LobbyState())
            return
        }

        local current_ms = getTickCount()
        local delta_ms = current_ms - this._last_time_ms
        
        this._last_time_ms = current_ms
        this._time_left_ms -= delta_ms

        this._validatePlayersArea()

        if (this._stage == StageType.HIDING) {
            this._updateStageHiding(delta_ms)
        } else if (this._stage == StageType.HUNTING) {
            this._updateStageHunting(delta_ms)
        }

        // Stage transitions
        if (this._time_left_ms <= 0) {
            if (this._stage == StageType.HIDING) {
                this._switchStageToHunting()
            } else if (this._stage == StageType.HUNTING) {
                this.machine.change(SummaryState(PlayerType.PROP))
            }
        }
    }

    </ packet = InvalidHitMessage />
    function onPlayerHunterInvalidAttack(pid, data) {
        if (isPlayerSpawned(pid) && !isPlayerDead(pid)) {
            local health = getPlayerHealth(pid) - HUNTER_SELF_DAMAGE
            setPlayerHealth(pid, health >= 0 ? health : 0)
        }
    }

    </ packet = PropUpdateClientMessage />
    function onPlayerPropUpdate(pid, data) {
        if (isPlayerSpawned(pid) && !isPlayerDead(pid)) {
            local packet = PropUpdateServerMessage(pid, data.vob_index, data.vob_height).serialize()
            packet.sendToAll(RELIABLE)
        }
    }

    </ event = "onPlayerJoin" />
    function onJoin(pid) {
        local player = server.players.get(pid)
        this._teleportPlayers([player], 500)

        // Assign and setup class
        if (this._stage == StageType.HIDING) {
            player.assigned_class = PlayerType.HUNTER
            this._players.hunters.push(player)

            // Update hounters
            local sync = RoundSynchronization(player_ids(server.players.online))
            sync.updateState({hunters_count = this._players.hunters.len()})
        } else if (this._stage == StageType.HUNTING) {
            player.assigned_class = PlayerType.SPECTATOR
            spawnPlayer(player.id)
        }

        classes.Setup(player)

        local sync = RoundSynchronization([player.id])
        sync.changeState(this._stage, this._map, this._time_left_ms, this._players.hunters.len(), this._players.props.len())
        sync.propsCreate(this._players.props)
    }

    </ event = "onPlayerDisconnect" />
    function onDisconnect(pid, reason) {
        local player = server.players.get(pid)

        // Remove player from local tracker
        local sync = RoundSynchronization(player_ids(server.players.online))
        if (player.assigned_class == PlayerType.HUNTER) {
            removeByValue(this._players.hunters, player)
            sync.updateState({hunters_count = this._players.hunters.len()})
        } else if (player.assigned_class == PlayerType.PROP) {
            removeByValue(this._players.props, player)
            sync.updateState({props_count = this._players.props.len()})
        }
    }

    </ event = "onPlayerRespawn" />
    function onRespawn(pid) {
        local player = server.players.get(pid)
        this._teleportPlayers([player], 500)

        if (this._stage == StageType.HIDING) {
            if (player.assigned_class != PlayerType.HUNTER) {
                spawnPlayer(player.id)
            }
        } else if (this._stage == StageType.HUNTING) {
            spawnPlayer(player.id)
        }

        classes.Setup(player)
    }

    </ event = "onPlayerHit" />
    function onGotHit(pid, kid, desc) {
        if (this._stage == StageType.HUNTING) {
            local attacker = server.players.get(kid)
            local victim = server.players.get(pid)

            if (attacker && victim &&
                attacker.assigned_class == PlayerType.HUNTER &&
                victim.assigned_class == PlayerType.PROP) {
                eventValue(-HUNTER_PROP_DAMAGE)
                return
            }
        }

        cancelEvent()
    }

    </ event = "onPlayerDead" />
    function onDead(pid, kid) {
        if (this._stage == StageType.HUNTING) {
            local victim = server.players.get(pid)
            if (victim) {
                if (victim.assigned_class == PlayerType.PROP) {
                    victim.assigned_class = PlayerType.SPECTATOR
                    removeByValue(this._players.props, victim)

                    local sync = null
                    sync = RoundSynchronization([victim.id])
                    sync.updateState({player_type = victim.assigned_class})

                    sync = RoundSynchronization(player_ids(server.players.online))
                    sync.updateState({props_count = this._players.props.len()})
                    sync.propsDestroy([victim])

                } else if (victim.assigned_class == PlayerType.HUNTER) {
                    victim.assigned_class = PlayerType.SPECTATOR
                    removeByValue(this._players.hunters, victim)

                    local sync = RoundSynchronization([victim.id])
                    sync.updateState({player_type = victim.assigned_class})

                    sync = RoundSynchronization(player_ids(server.players.online))
                    sync.updateState({hunters_count = this._players.hunters.len()})
                }
            }
        }
    }

    </ event = "onPlayerMessage" />
    function onMessage(pid, text) {
        local player = server.players.get(pid)
        if (player.assigned_class == PlayerType.SPECTATOR) {
            foreach (receiver in server.players.online) {
                if (receiver.assigned_class == PlayerType.SPECTATOR) {
                    sendPlayerMessageToPlayer(pid, receiver.id, 235, 235, 235, text)
                }
            }
        } else {
            foreach (receiver in server.players.online) {
                sendPlayerMessageToPlayer(pid, receiver.id, 235, 235, 235, text)
            }
        }
        
        print(getPlayerName(pid) + ": " + text)
    }
}