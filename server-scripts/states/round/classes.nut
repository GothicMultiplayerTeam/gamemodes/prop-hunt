classes <- {
    function Setup(player) {
        switch (player.assigned_class) {
        case PlayerType.HUNTER: this.Hunter(player); break
        case PlayerType.PROP: this.Prop(player); break
        case PlayerType.SPECTATOR: this.Spectator(player); break
        }
    }

    function Hunter(player) {
        setPlayerInstance(player.id, "PC_HERO")
        setPlayerMaxHealth(player.id, HUNTER_HEALT)
        setPlayerHealth(player.id, HUNTER_HEALT)
        setPlayerStrength(player.id, 100)
        setPlayerSkillWeapon(player.id, 0, 60)
        setPlayerSkillWeapon(player.id, 1, 60)

        giveItem(player.id, Items.id("SANTA_HAT"), 1)
        giveItem(player.id, Items.id("ITAR_KDF_L"), 1)
        giveItem(player.id, Items.id("ITMW_ADDON_STAB01"), 1)
        equipItem(player.id, Items.id("SANTA_HAT"))
        equipItem(player.id, Items.id("ITAR_KDF_L"))
        equipItem(player.id, Items.id("ITMW_ADDON_STAB01"))
        applyPlayerOverlay(player.id, Mds.id("HumanS_Sprint.mds"))

        setPlayerInvisible(player.id, false)
        setPlayerColor(player.id, 255, 50, 0)
    }

    function Prop(player) {
        setPlayerInstance(player.id, PROP_INSTANCE)
        setPlayerMaxHealth(player.id, PROP_HEALTH)
        setPlayerHealth(player.id, PROP_HEALTH)

        setPlayerInvisible(player.id, false)
        setPlayerColor(player.id, 68, 183, 255)
    }

    function Spectator(player) {
        setPlayerInstance(player.id, "PC_HERO")
        giveItem(player.id, Items.id("ITAR_JUDGE"), 1)
        equipItem(player.id, Items.id("ITAR_JUDGE"))
        applyPlayerOverlay(player.id, Mds.id("HumanS_Sprint.mds"))

        setPlayerInvisible(player.id, true)
        setPlayerColor(player.id, 157, 162, 165)
    }
}