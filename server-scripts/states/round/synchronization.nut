class RoundSynchronization {
    _receivers = null

    constructor(receivers) {
        this._receivers = receivers
    }

    function changeState(stage, map, time_left, hunters, props) {
        local message = SwitchToRoundMessage()
        message.stage_type = stage
        message.map_index = map.index
        message.time_left_ms = time_left
        message.hunters_count = hunters
        message.props_count = props

        foreach (pid in this._receivers) {
            local player = server.players.get(pid)
            if (player) {
                message.player_type = player.assigned_class
                message.serialize().send(pid, RELIABLE_ORDERED)
            }
        }
    }

    function updateState(data) {
        local message = RoundUpdateMessage()
        if ("time_left_ms" in data) {
            message.time_left_ms = data.time_left_ms
        }

        if ("player_type" in data) {
            message.player_type = data.player_type
        }

        if ("stage_type" in data) {
            message.stage_type = data.stage_type
        }

        if ("hunters_count" in data) {
            message.hunters_count = data.hunters_count
        }

        if ("props_count" in data) {
            message.props_count = data.props_count
        }
        
        foreach (pid in this._receivers) {
            message.serialize().send(pid, RELIABLE)
        }
    }

    function propsCreate(props) {
        foreach (prop in props) {
            local packet = PropCreateMessage(prop.id, prop.vob_index).serialize()
            foreach (pid in this._receivers) {
                packet.send(pid, RELIABLE_ORDERED)
            }
        }
    }

    function propsDestroy(props) {
        foreach (prop in props) {
            local packet = PropDestroyMessage(prop.id).serialize()
            foreach (pid in this._receivers) {
                packet.send(pid, RELIABLE)
            }
        }
    }

    function playTaunt(prop, sound_index) {
        local packet = TauntPlayServerMessage(prop.id, sound_index).serialize()
        foreach (pid in this._receivers) {
            packet.send(pid, RELIABLE)
        }
    }
}