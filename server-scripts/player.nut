class Player {
    id = -1
    assigned_class = null

    // PROP members:
    vob_index = 0

    constructor(id) {
        this.id = id
    }
}

class PlayerManager {
    _players = null
    online = null

    constructor(max_slots) {
        this._players = array(max_slots)
        this.online = []
    }

    function get(pid) {
        return this._players[pid]
    }

    function insert(pid) {
        local player = Player(pid)

        this._players[pid] = player
        this.online.push(player)
    }

    function remove(pid) {
        this._players[pid] = null

        foreach (i, player in this.online) {
            if (player.id == pid) {
                this.online[i] = this.online[this.online.len() - 1]
                this.online.pop()
                return
            }
        }
    }
}