class Client {
    states = null

    constructor() {
        this.states = BStateMachine()
    }
}

/////////////////////////////////////////
///	Events
/////////////////////////////////////////

local function init_handler() {
    // Client configuration
    enableEvent_Render(true)
    enable_WeaponTrail(true)
    enable_DamageAnims(false)

    client <- Client()
    // Initial state
    client.states.push(InitState())
}

addEventHandler("onInit", init_handler)