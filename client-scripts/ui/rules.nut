class RulesBoardUI extends Texture {
    _title = null
	_labels = null

    margin = 50

	constructor(lines) {
		base.constructor(0, 0, 0, 0, "DLG_CONVERSATION.TGA")

        this._labels = this._createDraws(lines)
        local size = this._calculateSize()
        this.setSize(size.width + this.margin * 2, size.height + this.margin * 2)

        this._title = Draw(0, 0, "Rules:")
        this._title.font = "FONT_OLD_20_WHITE_HI.TGA"
	}

    function _createDraws(lines) {
        local draws = []
        foreach (line in lines) {
            local draw = Draw(0, 0, line)
            draws.push(draw)
        }

        return draws
    }

    function _centerDraws(x, y, size) {
        local height = this.margin
        local center = (this.margin * 2 + size.width) / 2

        foreach (label in this._labels) {
            label.setPosition(x + (center - label.width / 2), y + height)
            height += label.height
        }
    }

    function _calculateSize() {
        local width = 0, height = 0
        foreach (label in this._labels) {
            width = max(width, label.width)
            height += label.height
        }

        return {width = width, height = height}
    }

    function setPosition(x, y) {
        base.setPosition(x, y)

        local size = this._calculateSize()
        this._title.setPosition(x + (size.width / 2 - this._title.width / 2), y - this._title.height)

        this._centerDraws(x, y, size)
    }

    function setPositionPx(x, y) {
        base.setPositionPx(x, y)
        
        local size = this._calculateSize()
        this._title.setPositionPx(x + nax(size.width / 2 - this._title.width / 2), y - this._title.heightPx)

        this._centerDraws(nax(x), nay(y), size)
    }

    function setVisible(toggle) {
        this.visible = toggle
        this._title.visible = toggle

        foreach (label in this._labels) {
            label.visible = toggle
        }
    }

	function clear() {
		foreach (label in this._labels) {
			label.text = ""
		}
	}
}