class GameInfoPage {
    _draws = null
    _duration_ms = 0
    _elapsed_ms = 0

    constructor(lines, duration_ms) {
        this._duration_ms = duration_ms

        this._draws = this._createDraws(lines)
        this._centerDraws()
    }

    function _createDraws(lines) {
        local draws = []
        foreach (line in lines) {
            local draw = Draw(0, 0, line)
            draw.font = "FONT_OLD_20_WHITE_HI.TGA"

            draws.push(draw)
        }

        return draws
    }

    function _centerDraws() {
        local height = 0
        foreach (draw in this._draws) {
            height += draw.height
        }

        foreach (draw in this._draws) {
            draw.setPosition(4096 - draw.width / 2, 4096 - height)
            height -= draw.height
        }
    }

    function _calculateAlpha() {
        if (this._elapsed_ms <= 300) {
            return smoothstep(0, 300, this._elapsed_ms) * 255
        } else if (this._elapsed_ms >= this._duration_ms - 300) {
            return (1.0 - smoothstep(this._duration_ms - 300, this._duration_ms, this._elapsed_ms)) * 255
        }

        return 255
    }

    function setVisible(toggle) {
        foreach (draw in this._draws) {
            draw.visible = toggle
        }
    }

    function setColor(r, g, b) {
        foreach (draw in this._draws) {
            draw.setColor(r, g, b)
        }
    }

    function finished() {
        return this._elapsed_ms >= this._duration_ms
    }

    function update(delta_ms) {
        this._elapsed_ms += delta_ms
        
        local alpha = this._calculateAlpha()
        foreach (draw in this._draws) {
            draw.alpha = alpha
        }
    }
}

class GameInfoUI {
    _queue = null
    _current = null
    _color = null
    _visible = false

    constructor() {
        this._queue = []
        this._current = null
        this._color = { r = 255, g = 255, b = 255 }
    }

    function setVisible(toggle) {
        this._visible = toggle

        if (this._current) {
            this._current.setVisible(toggle)
        }
    }

    function setColor(r, g, b) {
        this._color = { r = r, g = g, b = b }

        if (this._current) {
            this._current.setColor(r, g, b)
        }
    }

    function push(lines, duration_ms) {
        this._queue.push({ lines = lines, duration_ms = duration_ms })
    }

    function empty() {
        return this._queue.len() == 0
    }

    function update(delta_ms) {
        if (!this._current && this._queue.len() > 0) {
            local element = this._queue[0]
            this._queue.remove(0)

            this._current = GameInfoPage(element.lines, element.duration_ms)
            this._current.setColor(this._color.r, this._color.g, this._color.b)
            this._current.setVisible(this._visible)
        }

        if (this._current) {
            this._current.update(delta_ms)
            if (this._current.finished()) {
                this._current = null
            }
        }
    }
}