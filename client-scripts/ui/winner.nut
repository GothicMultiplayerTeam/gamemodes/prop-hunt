class WinnerUI {
    _label = null
    _team = null

    constructor(player_type) {
        this._label = Draw(0, 0, "Winner:")
        this._label.font = "FONT_OLD_20_WHITE_HI.TGA"
        this._label.setPosition(4096 - this._label.width / 2, 4096 - this._label.height)

        this._team = Draw(0, 0, "")
        this._team.font = "FONT_OLD_20_WHITE_HI.TGA"
        this._team.setScale(1.5, 1.5)

        if (player_type == PlayerType.HUNTER) {
            this._team.text = "Hunters"
            this._team.setColor(255, 50, 0)
            this._label.setColor(255, 50, 0)
        } else if (player_type == PlayerType.PROP) {
            this._team.text = "Props"
            this._team.setColor(68, 183, 255)
            this._label.setColor(68, 183, 255)
        }

        this._team.setPosition(4096 - this._team.width / 2, 4096)
    }

    function setVisible(toggle) {
        this._label.visible = toggle
        this._team.visible = toggle
    }
}
