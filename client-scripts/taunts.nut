class TauntRegistry {
    entries = null

    constructor() {
        this.entries = []
    }

    function getById(idx) {
        return this.entries[idx]
    }

    function getByName(name) {
        foreach (idx, taunt in this.entries) {
            if (name == taunt) {
                return idx
            }
        }

        return -1
    }

    function register(name) {
        this.entries.push(name)
    }
}

// Register all taunts here!
taunt_registry <- TauntRegistry()
taunt_registry.register("SVM_1_WhatDidYouDoInThere.wav")
taunt_registry.register("SVM_1_WhyAreYouInHere.wav")
taunt_registry.register("SVM_1_CheerFriend01.wav")
taunt_registry.register("SVM_1_CheerFriend02.wav")
taunt_registry.register("SVM_1_SpareMe.wav")