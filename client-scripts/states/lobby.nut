// Hunter losing HP.
// Information about eliminations on chat (Bimbol was eliminated.)

local rules = [
    "PropHunt is a gamemode in which you become a prop or a hunter.",
    "The props are disguised as objects and are trying to hide from",
    "hunters to avoid death. The hunters role is to eliminate all"
    "props on the map.",
    "",
    "Game is divided into two stages, hiding and hunting.",
    "In hiding stage, props have to hide around map and hunters",
    "must wait. In hunting stage, hunters have limited time",
    "to find every prop.",
    "",
    "To make searching easier, after every 30 seconds,",
    "randomly chosen props on the map will play sound.",
    "Also hunter can lose 5 health, every time they hit",
    "object that is not a prop.",
    "",
    "Hunters win the round if all props are killed,",
    "and props win if the time runs out or all hunters die."
]

class LobbyState extends BState {
    _rules = null
    _board = null
    _map = null
    _time_left_ms = -1

    constructor(data) {
        this._time_left_ms = data.time_left_ms
        this._map = map_registry.entries[data.map_index]
        this._rules = RulesBoardUI(rules)

        this._board = BoardLobbyUI()
        this._board.updateTimer(this._time_left_ms / 1000)
        this._board.updateMap(this._map.name)
        this._board.updatePlayers(getPlayersCount())

        // Position board in the right side of the screen
        local size = this._board.getSize()
        this._board.setPosition(8192 - size.width, 4096 - size.height / 2)

        local size = this._rules.getSize()
        this._rules.setPosition(4096 - size.width  / 2, 4096 - size.height / 2)
    }

    function onEnter() {
        this._rules.setVisible(true)
        this._board.setVisible(true)

        // Change game status
        setFreeze(true)
        setHudMode(HUD_ALL, HUD_MODE_HIDDEN)

        // Setup camera
        local camera = this._map.camera
        
        Camera.movementEnabled = false
        Camera.setPosition(camera.x, camera.y, camera.z)
        Camera.setRotation(25, camera.angle, 0)
    }

    function onExit() {
        this._rules.setVisible(false)
        this._board.setVisible(false)

        // Change game status
        setFreeze(false)
        setHudMode(HUD_ALL, HUD_MODE_DEFAULT)

        // Setup camera
        Camera.movementEnabled = true
    }

    </ packet = SwitchToRoundMessage />
    function onSwitchToRound(data) {
        this.machine.change(RoundState(data))
    }

    </ packet = LobbyUpdateMessage />
    function onBoardUpdate(data) {
        if (data.time_left_ms != null) {
            this._time_left_ms = data.time_left_ms
            this._board.updateTimer(data.time_left_ms / 1000)
        }
        
        if (data.map_index != null) {
            this._map = map_registry.entries[data.map_index]
            this._board.updateMap(this._map.name)
        }
    }

    </ event = "onPlayerCreate" />
    function onPlayerCreate(pid) {
        this._board.updatePlayers(getPlayersCount())
    }

    </ event = "onPlayerDestroy" />
    function onPlayerDestroy(pid) {
        this._board.updatePlayers(getPlayersCount() - 1)
    }

    </ event = "onRender" />
    function onRender() {
        this._time_left_ms -= WorldTimer.frameTime
        this._board.updateTimer(this._time_left_ms / 1000)
    }

    </ event = "onRespawn" />
    function onRespawn() {
        // fix camera reset bug while respawning after round being ended
        onEnter()
    }
}