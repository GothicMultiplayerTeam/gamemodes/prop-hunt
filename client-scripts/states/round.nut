class RoundState extends BState {
    _map = null
    _board = null
    _winner = null
    _props = null
    _infos = null
    _stages = null
    _controller = null
    _player_type = null
    _time_left_ms = -1

    constructor(data) {
        this._time_left_ms = data.time_left_ms
        this._player_type = data.player_type
        this._map = map_registry.entries[data.map_index]

        this._props = VobFollowerManager()
        this._stages = BStateMachine()

        this._infos = this._createInfos(data.player_type)
        this._board = this._createBoard(data.player_type, data.stage_type, data.hunters_count, data.props_count)
        this._controller = this._createController(data.player_type)

        if (data.stage_type == StageType.HIDING) {
            this._stages.push(HidingStage(data.player_type, this._infos, this._props))
        } else if (data.stage_type == StageType.HUNTING) {
            this._stages.push(HuntingStage(data.player_type, this._infos, this._props))
        }
    }

    function _createBoard(player_type, stage_type, hunters, props) {
        local board = BoardRoundUI()
        board.updateTimer(this._time_left_ms / 1000)
        board.updateStage(stage_type)
        board.updateClass(player_type)
        board.updateHunters(hunters)
        board.updateProps(props)

        // Position board in the right side of the screen
        local size = board.getSize()
        board.setPosition(8192 - size.width, 4096 - size.height / 2)

        return board
    }

    function _createInfos(player_type) {
        local infos = GameInfoUI()
        if (player_type == PlayerType.HUNTER) {
            infos.setColor(255, 50, 0)
        } else if (player_type == PlayerType.PROP) {
            infos.setColor(68, 183, 255)
        }

        return infos
    }

    function _createController(player_type) {
        switch (player_type) {
        case PlayerType.PROP: return PropController(this._map.vobs)
        case PlayerType.HUNTER: return HunterController(this._props)
        case PlayerType.SPECTATOR: return SpectatorController()
        }

        return null
    }

    function onEnter() {
        this._board.setVisible(true)
        this._infos.setVisible(true)
        this._controller.init()
    }

    function onExit() {
        this._board.setVisible(false)
        this._infos.setVisible(false)

        this._controller.dispose()
        this._props.clear()
    }

    </ packet = SwitchToLobbyMessage />
    function onSwitchToLobby(data) {
        this._stages.pop() // Pop stage state
        this.machine.change(LobbyState(data))
    }

    </ packet = RoundUpdateMessage />
    function onStateUpdate(data) {
        if (data.time_left_ms != null) {
            this._time_left_ms = data.time_left_ms
            this._board.updateTimer(this._time_left_ms / 1000)
        }

        if (data.player_type != null) {
            this._player_type = data.player_type
            this._board.updateClass(data.player_type)

            this._controller.dispose()
            this._controller = this._createController(data.player_type)
            this._controller.init()
        }
        
        if (data.hunters_count != null) {
            this._board.updateHunters(data.hunters_count)
        }

        if (data.props_count != null) {
            this._board.updateProps(data.props_count)
        }
        
        if (data.stage_type != null) {
            this._board.updateStage(data.stage_type)

            if (data.stage_type == StageType.HIDING) {
                this._stages.change(HidingStage(this._player_type, this._infos, this._props))
            } else if (data.stage_type == StageType.HUNTING) {
                this._stages.change(HuntingStage(this._player_type, this._infos, this._props))
            }
        }
    }

    </ packet = PropCreateMessage />
    function onPlayerPropCreate(data) {
        // Local player has custom vob controller, so skip
        if (data.idx != heroId) {
            local vob_data = this._map.vobs[data.vob_index]
            this._props.insert(data.idx, vob_data.visual)
        }
    }

    </ packet = PropDestroyMessage />
    function onPlayerPropDestroy(data) {
        local follower = this._props.getById(data.idx)
        if (follower) {
            Chat.print(68, 183, 255, getPlayerName(data.idx) + " was eliminated.")
            if (isPlayerStreamed(data.idx)) {
                // Spawn Beliar thunderbolt effect when prop dies
                addEffect(data.idx, "SPELLFX_BELIARSRAGE")
            }
            
            this._props.remove(data.idx)
        }
    }

    </ packet = PropUpdateServerMessage />
    function onPlayerPropUpdate(data) {
        local follower = this._props.getById(data.idx)
        if (follower) {
            if (data.vob_index != null) {
                local vob_data = this._map.vobs[data.vob_index]
                follower.setVisual(vob_data.visual)
            }
            
            if (data.vob_height != null) {
                follower.height_offset = data.vob_height
            }
        }
    }

    </ packet = SummaryMessage />
    function onSummary(data) {
        this._winner = WinnerUI(data.winner_type)
        this._winner.setVisible(true)

        sounds.finish_game.play()
    }

    </ event = "onRender" />
    function onRender() {
        this._time_left_ms -= WorldTimer.frameTime
        this._board.updateTimer(this._time_left_ms / 1000)

        this._props.update()
        this._infos.update(WorldTimer.frameTime)
        this._controller.update(WorldTimer.frameTime)

        this._stages.current().onUpdate(WorldTimer.frameTime, this._time_left_ms)
    }

    </ event = "onKey" />
    function onKey(key) {
        this._controller.onInput(key)
    }

    </ event = "onMouseClick" />
    function onMouseClick(btn) {
        this._controller.onMouseClick(btn)
    }

    </ event = "onMouseMove" />
    function onMouseMove(y, x) {
        this._controller.onMouseMove(x, y)
    }

    </ event = "onMouseWheel" />
    function onMouseWheel(value) {
        this._controller.onMouseWheel(value)
    }

    </ event = "onAnim" />
    function onAnim(id) {
        this._controller.onAnim(id)
    }

    </ event = "onFocusCollect" />
    function onFocusCollect(focuses) {
        this._controller.onFocusCollect(focuses)
    }

    </ event = "onPlayerSpawn" />
    function onPlayerSpawn(pid) {
        local follower = this._props.getById(pid)
        // Disable player collision
        if (follower) {
            setPlayerCollision(pid, false)
        }
    }

    </ event = "prop:onVisual" />
    function onPropVisualUpdate(vob_index, height) {
        if (this._player_type == PlayerType.PROP) {
            local sync = this._controller.synchronization
            sync.current_vob_index = vob_index
            sync.current_vob_height = height
        }
    }
}