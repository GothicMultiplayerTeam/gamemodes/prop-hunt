class InitState extends BState {
    _label = null

    constructor() {
        this._label = Draw(0, 0, "Initializing server data, please wait...")
        this._label.font = "FONT_OLD_20_WHITE_HI.TGA"
        this._label.setPosition(4096 - this._label.width / 2, 4096 - this._label.height / 2)
    }

    function onEnter() {
        this._label.visible = true
        Chat.setVisible(false)

        // Change game status
        setFreeze(true)
        setHudMode(HUD_ALL, HUD_MODE_HIDDEN)
        clearMultiplayerMessages()

        // Setup camera
        Camera.movementEnabled = false
        Camera.setPosition(2500, 3000, 0)
        Camera.setRotation(25.0, 90.6825, 0)
    }

    function onExit() {
        this._label.visible = false
        Chat.setVisible(true)

        // Change game status
        setFreeze(false)
        setHudMode(HUD_ALL, HUD_MODE_DEFAULT)

        // Setup camera
        Camera.movementEnabled = true
    }

    </ packet = SwitchToLobbyMessage />
    function onSwitchToLobby(data) {
        this.machine.change(LobbyState(data))
    }

    </ packet = SwitchToRoundMessage />
    function onSwitchToRound(data) {
        this.machine.change(RoundState(data))
    }
}
