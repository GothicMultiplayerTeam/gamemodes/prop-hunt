addEvent("hunter:onAttack")

class HunterController extends PlayerController {
    _current_target = null
    _props = null
    _ui = null

    constructor(available_props) {
        this._props = available_props
        this._ui = HunterUI()
    }

    function _traceRayCursor(x, y) {
        local p1 = Camera.backProject(x, y, 0)
        local p2 = Camera.backProject(x, y, 1500)

        local position = Vec3(p1.x, p1.y, p1.z)
        local direction = Vec3(p2.x - p1.x, p2.y - p1.y, p2.z - p1.z)

        local result = GameWorld.traceRayNearestHit(position, direction, TRACERAY_VOB_IGNORE_PROJECTILES | TRACERAY_VOB_IGNORE_CHARACTER)
        if (result && result.vob && getVobType(result.vob) != -1) {
            return result.vob
        }

        return null
    }

    function init() {
        this._ui.controls.visible = true
        setCursorVisible(true)
    }

    function dispose() {
        this._ui.controls.visible = false
        setCursorVisible(false)
    }

    function update(delta_ms) {
        local target_locked = isLogicalKeyPressed(GAME_ACTION)
        if (target_locked && this._current_target) {
            // Lock target
            local p1 = this._current_target.getPosition()
            local p2 = getPlayerPosition(heroId)
            local angle = getVectorAngle(p2.x, p2.z, p1.x, p1.z)

            setPlayerAngle(heroId, angle)
        }

        if (this._current_target) {
            local p1 = this._current_target.getPosition()
            local p2 = getPlayerPosition(heroId)

            if (getDistance3d(p1.x, p1.y, p1.z, p2.x, p2.y, p2.z) <= 1000) {
                local position = this._current_target.getPosition()
                this._ui.target_name.setWorldPosition(position.x, position.y, position.z)
            } else {
                this._current_target = null
                this._ui.target_name.visible = false
            }
        }
    }

    function onMouseClick(btn) {
        if (btn == MOUSE_BUTTONRIGHT) {
            local cursor = getCursorPositionPx()

            // Acquire new target
            local new_target = this._traceRayCursor(cursor.x, cursor.y)
            if (!this._current_target || this._current_target.ptr != new_target) {
                if (new_target) {
                    this._current_target = Vob(new_target)
                    this._ui.target_name.updateText(0, this._current_target.visual)
                } else {
                    this._current_target = null
                }

                this._ui.target_name.visible = new_target != null
            }
        }
    }

    function onAnim(id) {
        if (this._current_target) {
            local name = getPlayerAniNameById(heroId, id)
            if (name.find("ATTACK") != null) {
                local p1 = getPlayerPosition(heroId)
                local p2 = this._current_target.getPosition()

                if (getDistance3d(p1.x, p1.y, p1.z, p2.x, p2.y, p2.z) <= PROP_HIT_DISTANCE) {
                    local follower = this._props.getByPtr(this._current_target.ptr)
                    callEvent("hunter:onAttack", follower)
                }
            }
        }
    }

    function onFocusCollect(focuses) {
        focuses.clear()
    }
}