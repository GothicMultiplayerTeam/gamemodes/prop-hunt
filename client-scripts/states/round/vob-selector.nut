class VobSelector {
    index = -1
    _possible_vobs = null

    constructor(vobs) {
        this.index = 0
        this._possible_vobs = vobs
    }

    function selected() {
        return this._possible_vobs[this.index]
    }

    function handleInput(key) {
        if (key == KEY_LEFT) {
            if (--this.index < 0) {
                this.index = this._possible_vobs.len() - 1
            }

            return true

        } else if (key == KEY_RIGHT) {
            if (++this.index >= this._possible_vobs.len()) {
                this.index = 0
            }
            
            return true
        }

        return false
    }
}