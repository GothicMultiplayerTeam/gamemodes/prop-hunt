local instance = null
local ray_length = 8192

local points = []

class AreaPoint {
    vob = null
    line = null

    constructor(vob, line) {
        this.vob = vob
        this.line = line
    }
}

class AreaCreator {
    points = null
    _selected_point = null
    _selected_index = -1
    _is_moving = false

    constructor(points) {
        this.points = []

        foreach (i, point in points) {
            this._addPoint(point, i)
        }
    }

    function _traceRayCursor(x, y, distance, flags) {
        local p1 = Camera.backProject(x, y, 0)
        local p2 = Camera.backProject(x, y, distance)

        local position = Vec3(p1.x, p1.y, p1.z)
        local direction = Vec3(p2.x - p1.x, p2.y - p1.y, p2.z - p1.z)

        return GameWorld.traceRayNearestHit(position, direction, flags)
    }

    function _addPoint(position, index) {
        local vob = Vob("LIGHT.3DS")
        vob.setPosition(position.x, position.y, position.z)
        vob.addToWorld()

        local last_position = position
        if (this.points.len() > 0) {
            local last_point = this.points[index - 1]
            last_position = last_point.vob.getPosition()

            local next_point = index < this.points.len() ? this.points[index] : this.points[0]
            next_point.line.setBegin(position.x, position.y, position.z)
        }

        local line = Line3d(last_position.x, last_position.y, last_position.z, position.x, position.y, position.z)
        line.visible = true

        local new_point = AreaPoint(vob, line)
        if (index < this.points.len()) {
            this.points.insert(index, new_point)
        } else {
            this.points.push(new_point)
        }

        return new_point
    }

    function _removePoint(index) {
        local next_point = index + 1 < this.points.len() ? this.points[index + 1] : this.points[0]
        local prev_point = index - 1 >= 0 ? this.points[index - 1] : this.points.top()

        local position = prev_point.vob.getPosition()
        next_point.line.setBegin(position.x, position.y, position.z)

        this.points.remove(index)
    }

    function _selectPoint(x, y) {
        // Check if hit valid vob type
        local result = this._traceRayCursor(x, y, ray_length, TRACERAY_VOB_IGNORE_PROJECTILES | TRACERAY_VOB_IGNORE_CHARACTER)
        if (result && result.vob && getVobType(result.vob) != -1) {
            foreach (i, point in this.points) {
                if (point.vob.ptr == result.vob) {
                    this._selected_index = i
                    this._selected_point = point
                    this._selected_point.vob.drawBBox3d = true
                }
            }
        }

        return this._selected_point != null
    }

    function _movePoint(x, y) {
        if (this._selected_point) {
            local result = this._traceRayCursor(x, y, ray_length, TRACERAY_VOB_IGNORE)
            if (result) {
                this._selected_point.vob.beginMovement()
                this._selected_point.vob.setPosition(result.intersect.x, result.intersect.y, result.intersect.z)
                this._selected_point.vob.endMovement()

                this._selected_point.line.setEnd(result.intersect.x, result.intersect.y, result.intersect.z)

                local next_point = this._selected_index + 1 < this.points.len() ? this.points[this._selected_index + 1] : this.points[0]
                next_point.line.setBegin(result.intersect.x, result.intersect.y, result.intersect.z)
            }
        }
    }

    function enter() {
        setHudMode(HUD_ALL, HUD_MODE_HIDDEN)
        setCursorVisible(true)
        enableFreeCam(true)
        enableEvent_Render(true)

        setPlayerPosition(heroId, 13054.1, 998.185, -2402.96)
        Camera.setPosition(13054.1, 998.185, -2402.96)
    }

    function exit() {
        setHudMode(HUD_ALL, HUD_MODE_DEFAULT)
        setCursorVisible(false)
        enableFreeCam(false)
    }

    function onInput(key) {
        if (key == KEY_G || key == KEY_H) {
            local cursor = getCursorPositionPx()
            local result = this._traceRayCursor(cursor.x, cursor.y, ray_length, TRACERAY_VOB_IGNORE)

            if (result) {
                local index = key == KEY_G ? this.points.len() : this._selected_index + 1
                if (this._selected_point) {
                    this._selected_point.vob.drawBBox3d = false
                }

                this._selected_index = index
                this._selected_point = this._addPoint(result.intersect, index)
                this._selected_point.vob.drawBBox3d = true
            }
        } else if (key == KEY_DELETE) {
            if (this._selected_index != -1) {
                this._removePoint(this._selected_index)

                this._selected_index = -1
                this._selected_point = null
            }
        }
    }

    function onClick(btn) {
        if (btn == MOUSE_BUTTONLEFT) {
            // Reset current selection
            if (this._selected_point) {
                this._selected_point.vob.drawBBox3d = false
                this._selected_point = null
                this._selected_index = -1
            }

            local cursor = getCursorPositionPx()
            if (this._selectPoint(cursor.x, cursor.y)) {
                this._is_moving = true
            }
        }
    }

    function onRelease(btn) {
        if (btn == MOUSE_BUTTONLEFT) {
            this._is_moving = false
        }
    }

    function onMove(x, y) {
        local free_cam = isMouseBtnPressed(MOUSE_BUTTONRIGHT)

        pauseFreeCam(!free_cam)
        setCursorVisible(!free_cam)

        if (this._is_moving) {
            local cursor = getCursorPositionPx()
            this._movePoint(cursor.x, cursor.y)
        }
    }
}

/////////////////////////////////////////
///	Events
/////////////////////////////////////////

local function cmd_handler(cmd, params) {
    if (cmd == "create-area") {
        if (instance) {
            instance.exit()
            instance = null
        } else {
            instance = AreaCreator(points)
            instance.enter()
        }
    } else if (instance) {
        if (cmd == "dump") {
            foreach (point in instance.points) {
                local p = point.vob.getPosition()
                print("{ x = " + p.x + ", y = " + p.y + ", z = " + p.z + " },")
            }
        }
    }
}

addEventHandler("onCommand", cmd_handler)

local function key_handler(key) {
    if (instance) {
        instance.onInput(key)
    }
}

addEventHandler("onKey", key_handler)

local function mouse_click_handler(btn) {
    if (instance) {
        instance.onClick(btn)
    }
}

addEventHandler("onMouseClick", mouse_click_handler)

local function mouse_release_handler(btn) {
    if (instance) {
        instance.onRelease(btn)
    }
}

addEventHandler("onMouseRelease", mouse_release_handler)

local function mouse_move_handler(x, y) {
    if (instance) {
        instance.onMove(x, y)
    }
}

addEventHandler("onMouseMove", mouse_move_handler)