function randomShuffle(elements) {
    local len = elements.len()
    for (local i = 0; i < len; ++i) {
        local index = rand() % len
        local value = elements[index]

        elements[index] = elements[i]
        elements[i] = value
    }
}

function removeByValue(elements, value) {
    foreach (i, element in elements) {
        if (element == value) {
            elements[i] = elements[elements.len() - 1]
            elements.pop()
            return
        }
    }
}

function max(v1, v2) {
    return v1 > v2 ? v1 : v2;
}

function min(v1, v2) {
    return v1 < v2 ? v1 : v2;
}

function smoothstep(edge0, edge1, x) {
    // normalize
    x = (x - edge0) / (edge1 - edge0)
    // clamp
    x = max(0.0, min(x, 1.0))

    return x * x * (3.0 - 2.0 * x)
}