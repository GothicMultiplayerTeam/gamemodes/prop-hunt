class MapRegistry {
    entries = null

    constructor() {
        this.entries = []
    }

    function _validate(data) {
        if (!("name" in data)) {
            throw "Missing 'name' key!"
        }
        
        if (!("world" in data)) {
            throw "Missing 'world' key!"
        }

        if (!("spawn" in data)) {
            throw "Missing 'teams' key!"
        }

        if (!("vobs" in data)) {
            throw "Missing 'bonuses' key!"
        }
    }

    function register(data) {
        this._validate(data)
        data.index <- this.entries.len()

        this.entries.push(data)
    }
}

// Register all definitions here!
map_registry <- MapRegistry()
