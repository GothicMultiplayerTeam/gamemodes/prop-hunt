// PlayerType
enum PlayerType {
    HUNTER,
    PROP,
    SPECTATOR
}

// StageType
enum StageType {
    HIDING,
    HUNTING
}