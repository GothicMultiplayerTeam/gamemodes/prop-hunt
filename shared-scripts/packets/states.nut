class SwitchToLobbyMessage extends BPacketMessage {
    </ type = BPacketInt32 />
    time_left_ms = -1

    </ type = BPacketInt8 />
    map_index = 0
}

class SwitchToRoundMessage extends BPacketMessage {
    </ type = BPacketInt8 />
    player_type = -1

    </ type = BPacketInt8 />
    stage_type = -1

    </ type = BPacketInt8 />
    map_index = 0

    </ type = BPacketInt32 />
    time_left_ms = -1

    </ type = BPacketUInt8 />
    hunters_count = 0

    </ type = BPacketUInt8 />
    props_count = 0
}