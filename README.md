# Introduction

**Prop Hunt** is a gamemode, which implements the gameplay in the form of battle that involves moving a flag from enemy base to home base to score points. In this gamemode the varius extra features were introduced like extra bonuses, which can be captured or level system which increases damage output for player.

# Rules

- To win round your team has to gain 2 points.

# Installation

Include `scripts.xml` to your project, this can be done by adding this line to your `config.xml` file.

Example:

```xml
<import src="gamemodes/prop-hunt/scripts.xml" />
```

# License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
